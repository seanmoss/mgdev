#### This contains some files that are useful for assisting in development of Monogame projects.


* `Monogame.Content.Builder.targets`
    * Fixes the issue where building content from Visual Studio would ignore the output settings in the .mgcb file, and always output the files to the wrong place. Just replace the file with the same name in the `Program Files (x86)\MSBuild\Monogame\v3.0` folder to apply the fix.
* `AdditionalProjectTransforms.xslt`
    * Example of how to copy xml into project definitions for Protobuild. This one in particular allows the use of ILRepack to combine assemblies to create standalone applications.
* `Software.md`
    * Contains a list of royalty free websites and downloadable software that can be used to develop content
