### Contains a list of software/websites used for content creation

* SpaceScape
    * http://alexcpeterson.com/spacescape/
    * Allows the detailed creation of skyboxes in space, including nebulas and whatnot
* planet1.js
    * http://wwwtyro.github.io/procedural.js/planet1/
    * Allows creation of random procedural planet textures, including normal maps and cloud maps
    * Resulting textures are sphere-mapped
