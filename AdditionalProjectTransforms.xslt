<Target Name="AfterBuild" Condition="<PROJECT CONDITION HERE>">
  <ItemGroup>
    <MergeAssemblies Include="$(OutputPath)\Program.exe" />
    <MergeAssemblies Include="$(OutputPath)\Library.dll" />
  </ItemGroup>
  <PropertyGroup>
    <OutputAssembly>$(OutputPath)\Program.Standalone.exe</OutputAssembly>
    <Merger Condition="('$(OS)' == 'Windows_NT')">"$(SolutionDir)\packages\ILRepack.2.0.10\tools\ILRepack.exe"</Merger>
    <Merger Condition="('$(OS)' != 'Windows_NT')">mono "$(SolutionDir)\packages\ILRepack.2.0.10\tools\ILRepack.exe"</Merger>
  </PropertyGroup>
  <Message Text="MERGING: @(MergeAssemblies->'%(Filename)') into $(OutputAssembly)" Importance="High" />
  <Exec Command="$(Merger) /out:&quot;$(OutputAssembly)&quot; @(MergeAssemblies->'&quot;%(FullPath)&quot;', ' ')" />
</Target>
